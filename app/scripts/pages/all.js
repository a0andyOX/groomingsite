(function($, window, document) {

  function addActive() {
    $('.addactive').addClass('active');
  }

  function bannerscroll() {
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
    new ScrollMagic.Scene({triggerElement: ".m-banner"})
    					.setTween(".m-banner img", {y: "50%", ease: Linear.easeNone})
    					.addTo(controller);
  }

  function menu() {
    $('.menu-burger').on('click', function() {
      $('header').toggleClass('open');
      $('header .nav').slideToggle();
    });
  }

  function smoothAnchor() {
    var $root = $('html, body');

    $('a[href^="#"]').click(function (e) {
        e.preventDefault();
        $root.animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 600);

        return false;
    });
  }

 $(function() {
   addActive();
   bannerscroll();
   menu();
   smoothAnchor();
 });
}(window.jQuery, window, document));
